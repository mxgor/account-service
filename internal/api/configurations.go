package api

import (
	"github.com/gin-gonic/gin"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/common"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/handlers"
)

func ConfiguationsRoutes(group *gin.RouterGroup, e common.Env) *gin.RouterGroup {
	configurationsGroup := group.Group("/configurations")

	configurationsGroup.GET("/list", common.ConstructResponse(handlers.GetConfigurations, e))
	configurationsGroup.GET("/getUserInfo", common.ConstructResponse(handlers.GetUserInfo, e))
	configurationsGroup.PUT("/save", common.ConstructResponse(handlers.SaveConfigurations, e))

	return configurationsGroup
}
