package api

import (
	"github.com/gin-gonic/gin"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/common"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/handlers"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/middleware"
)

const (
	presentationRequestEventMessage = "presentation request received over REST API"
	proofEventMessage               = "Created proof"
)

func PresentationsRoutes(group *gin.RouterGroup, e common.Env) *gin.RouterGroup {

	presentationGroup := group.Group("/presentations")

	presentationGroup.POST("/list", common.ConstructResponse(handlers.ListPresentations, e))

	presentationGroup.GET("/selection/:id", middleware.WithHistoryRecord(common.PresentationRequest,
		presentationRequestEventMessage, e),
		common.ConstructResponse(handlers.GetPresentationRequest, e),
	)
	presentationGroup.POST("/proof/:id", middleware.WithHistoryRecord(common.Presented,
		proofEventMessage, e),
		common.ConstructResponse(handlers.CreatePresentation, e),
	)
	presentationGroup.GET("/selection/all", common.ConstructResponse(handlers.GetPresentationDefinitions, e))

	return presentationGroup
}
