package api

import (
	"github.com/gin-gonic/gin"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/common"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/handlers"
)

func CredentialOfferRoutes(group *gin.RouterGroup, e common.Env) *gin.RouterGroup {
	offerGroup := group.Group("/offers")
	offerGroup.PUT("/create", common.ConstructResponse(handlers.CreateCredentialOffer, e))
	offerGroup.GET("/list", common.ConstructResponse(handlers.GetCredentialOffers, e))
	offerGroup.POST("/:id/accept", common.ConstructResponse(handlers.AcceptCredentialOffer, e))
	offerGroup.POST("/:id/deny", common.ConstructResponse(handlers.DenyCredentialOffer, e))
	return offerGroup
}
